import discord
import typing
from discord.ext import commands


class Moderation(commands.Cog):
    """Useful moderator commands that can come in handy"""
    def __init__(self, bot):
        self.bot = bot
        self._last_member = None

    @commands.command()
    @commands.has_permissions(ban_members=True)
    async def ban(self, ctx, member: typing.Optional[discord.Member], *, reason=None):
        """Bans a user from the guild"""
        setattr()
        if member == ctx.author:
            await ctx.send("uhhhh I'm not sure you want to do that Chief")
            return
        if member is None:
            await ctx.send("I have successfully banned no one")
        if member.top_role > ctx.author.top_role or member.top_role == ctx.author.top_role:
            await ctx.send(f"{member} has a higher role than you or has an equal role to you!")
            return
        if member.top_role > ctx.me.top_role or member.top_role == ctx.author.top_role:
            await ctx.send(f"{member} has a higher role than me or has an equal role to me. I cannot ban them")
            return
        await member.ban(reason=reason)
        await ctx.send(f"{member.name}#{member.discriminator} has been banned for the reason of: `{reason}`")

    @ban.error
    async def ban_error(self, ctx, error):
        if isinstance(error, commands.MissingPermissions):
            await ctx.send("I do not have permission to ban specified user")
        raise error

    @commands.command(aliases=["massban", "mass-ban"])
    @commands.has_permissions(administrator=True)
    async def mban(self, ctx, members: commands.Greedy[discord.Member], *, reason=None):
        """Bans multiple people from the guild. Requires Administrator permission"""
        for member in members:
            await member.ban(reason=reason)
        await ctx.send("Process Complete. Banned everyone specified")

    @commands.command()
    @commands.has_permissions(ban_members=True)
    async def unban(self, ctx, user: discord.User):
        """Unbans a member from a server. Only takes user ids"""
        await ctx.guild.unban(user)
        await ctx.send(f"Unbanned specified user {user.display_name}")

    @commands.group(aliases=["clear"])
    @commands.has_permissions(manage_messages=True)
    async def purge(self, ctx):
        """Purges x amount of messages."""
        if ctx.invoked_subcommand is None:
            e = discord.Embed(title="Wrong Usage!", description="Purge command has seperate options. The available options it has are:\n`!purge all` purges any message from the current channel with a given amount\n`!purge bots` purges messages sent from bots", color=0xff0000)
            await ctx.send(embed=e)

    @purge.command()
    async def all(self, ctx, amount: int):
        """Purges all messages"""
        if amount < 1001:
            await ctx.message.delete()
            await ctx.channel.purge(limit=amount)
            await ctx.channel.send(f"Purged {amount} messages from {ctx.channel.mention}", delete_after=3)
        else:
            await ctx.send("Amount must be 1000 messages or under")

    @purge.command(aliases=["bot"])
    async def bots(self, ctx, amount: int):
        """Purges messages from bots"""
        def is_bot(m):
            return m.author == self.bot.user
        if amount < 1001:
            await ctx.message.delete()
            await ctx.channel.purge(limit=amount, check=is_bot)
            await ctx.send(f"Purged {amount} bot messages from {ctx.channel.mention}", delete_after=3)
        else:
            await ctx.send("Amount must be 1000 messages or under")

    @purge.command(aliases=["embed"])
    async def embeds(self, ctx, amount: int):
        """Purges embed messages from bots, webhooks, or link embeds"""
        pass


def setup(bot):
    bot.add_cog(Moderation(bot))
